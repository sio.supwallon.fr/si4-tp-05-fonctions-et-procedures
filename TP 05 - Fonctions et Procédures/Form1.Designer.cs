﻿namespace TP_05___Fonctions_et_Procédures
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAuTravail = new System.Windows.Forms.Button();
            this.btnAddition = new System.Windows.Forms.Button();
            this.lblA = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.numA = new System.Windows.Forms.NumericUpDown();
            this.numB = new System.Windows.Forms.NumericUpDown();
            this.grpCalcul = new System.Windows.Forms.GroupBox();
            this.btnSoustraction = new System.Windows.Forms.Button();
            this.btnMultiplication = new System.Windows.Forms.Button();
            this.btnDivision = new System.Windows.Forms.Button();
            this.btnIncrement = new System.Windows.Forms.Button();
            this.btnSaluer = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnModifier = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numB)).BeginInit();
            this.grpCalcul.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAuTravail
            // 
            this.btnAuTravail.Location = new System.Drawing.Point(12, 12);
            this.btnAuTravail.Name = "btnAuTravail";
            this.btnAuTravail.Size = new System.Drawing.Size(75, 23);
            this.btnAuTravail.TabIndex = 0;
            this.btnAuTravail.Text = "Au travail";
            this.btnAuTravail.UseVisualStyleBackColor = true;
            this.btnAuTravail.Click += new System.EventHandler(this.btnAuTravail_Click);
            // 
            // btnAddition
            // 
            this.btnAddition.Location = new System.Drawing.Point(102, 57);
            this.btnAddition.Name = "btnAddition";
            this.btnAddition.Size = new System.Drawing.Size(75, 23);
            this.btnAddition.TabIndex = 1;
            this.btnAddition.Text = "Addition";
            this.btnAddition.UseVisualStyleBackColor = true;
            this.btnAddition.Click += new System.EventHandler(this.btnAddition_Click);
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(6, 21);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(14, 13);
            this.lblA.TabIndex = 2;
            this.lblA.Text = "A";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(6, 47);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(14, 13);
            this.lblB.TabIndex = 3;
            this.lblB.Text = "B";
            // 
            // numA
            // 
            this.numA.Location = new System.Drawing.Point(26, 19);
            this.numA.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numA.Name = "numA";
            this.numA.Size = new System.Drawing.Size(39, 20);
            this.numA.TabIndex = 4;
            this.numA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numB
            // 
            this.numB.Location = new System.Drawing.Point(26, 45);
            this.numB.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numB.Name = "numB";
            this.numB.Size = new System.Drawing.Size(39, 20);
            this.numB.TabIndex = 5;
            this.numB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grpCalcul
            // 
            this.grpCalcul.Controls.Add(this.lblA);
            this.grpCalcul.Controls.Add(this.numB);
            this.grpCalcul.Controls.Add(this.lblB);
            this.grpCalcul.Controls.Add(this.numA);
            this.grpCalcul.Location = new System.Drawing.Point(12, 41);
            this.grpCalcul.Name = "grpCalcul";
            this.grpCalcul.Size = new System.Drawing.Size(84, 82);
            this.grpCalcul.TabIndex = 6;
            this.grpCalcul.TabStop = false;
            this.grpCalcul.Text = "Calcul";
            // 
            // btnSoustraction
            // 
            this.btnSoustraction.Location = new System.Drawing.Point(102, 83);
            this.btnSoustraction.Name = "btnSoustraction";
            this.btnSoustraction.Size = new System.Drawing.Size(75, 23);
            this.btnSoustraction.TabIndex = 7;
            this.btnSoustraction.Text = "Soustraction";
            this.btnSoustraction.UseVisualStyleBackColor = true;
            this.btnSoustraction.Click += new System.EventHandler(this.btnSoustraction_Click);
            // 
            // btnMultiplication
            // 
            this.btnMultiplication.Location = new System.Drawing.Point(183, 57);
            this.btnMultiplication.Name = "btnMultiplication";
            this.btnMultiplication.Size = new System.Drawing.Size(78, 23);
            this.btnMultiplication.TabIndex = 8;
            this.btnMultiplication.Text = "Multiplication";
            this.btnMultiplication.UseVisualStyleBackColor = true;
            this.btnMultiplication.Click += new System.EventHandler(this.btnMultiplication_Click);
            // 
            // btnDivision
            // 
            this.btnDivision.Location = new System.Drawing.Point(183, 83);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(78, 23);
            this.btnDivision.TabIndex = 9;
            this.btnDivision.Text = "Division";
            this.btnDivision.UseVisualStyleBackColor = true;
            this.btnDivision.Click += new System.EventHandler(this.btnDivision_Click);
            // 
            // btnIncrement
            // 
            this.btnIncrement.Location = new System.Drawing.Point(12, 129);
            this.btnIncrement.Name = "btnIncrement";
            this.btnIncrement.Size = new System.Drawing.Size(75, 23);
            this.btnIncrement.TabIndex = 10;
            this.btnIncrement.Text = "Incrément";
            this.btnIncrement.UseVisualStyleBackColor = true;
            this.btnIncrement.Click += new System.EventHandler(this.btnIncrement_Click);
            // 
            // btnSaluer
            // 
            this.btnSaluer.Location = new System.Drawing.Point(12, 158);
            this.btnSaluer.Name = "btnSaluer";
            this.btnSaluer.Size = new System.Drawing.Size(75, 23);
            this.btnSaluer.TabIndex = 11;
            this.btnSaluer.Text = "Saluer";
            this.btnSaluer.UseVisualStyleBackColor = true;
            this.btnSaluer.Click += new System.EventHandler(this.btnSaluer_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 187);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 12;
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(118, 187);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(75, 23);
            this.btnModifier.TabIndex = 13;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnSaluer);
            this.Controls.Add(this.btnIncrement);
            this.Controls.Add(this.btnDivision);
            this.Controls.Add(this.btnMultiplication);
            this.Controls.Add(this.btnSoustraction);
            this.Controls.Add(this.grpCalcul);
            this.Controls.Add(this.btnAddition);
            this.Controls.Add(this.btnAuTravail);
            this.Name = "Form1";
            this.Text = "Fonctions & Procédures";
            ((System.ComponentModel.ISupportInitialize)(this.numA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numB)).EndInit();
            this.grpCalcul.ResumeLayout(false);
            this.grpCalcul.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAuTravail;
        private System.Windows.Forms.Button btnAddition;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.NumericUpDown numA;
        private System.Windows.Forms.NumericUpDown numB;
        private System.Windows.Forms.GroupBox grpCalcul;
        private System.Windows.Forms.Button btnSoustraction;
        private System.Windows.Forms.Button btnMultiplication;
        private System.Windows.Forms.Button btnDivision;
        private System.Windows.Forms.Button btnIncrement;
        private System.Windows.Forms.Button btnSaluer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnModifier;
    }
}

