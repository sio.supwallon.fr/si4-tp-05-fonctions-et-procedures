﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_05___Fonctions_et_Procédures
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAuTravail_Click(object sender, EventArgs e)
        {
            // todo 01 - Créer la procédure appelée "AuTravail" 
            //           elle ne prend aucun paramètre
            //           et affiche le message "Travail en cours..." dans une boîte de dialogue (MessageBox)

            // todo 02 - Appel de la procédure "AuTravail"
            //            * Appeler la procédure ici pour l'utiliser

        }

        private void btnAddition_Click(object sender, EventArgs e)
        {
            // toto 03 - Créer la fonction appelée "Addition"
            //           elle prend en paramètre deux valeurs A et B de type "decimal"
            //           et retourne l'addition des deux valeurs (donc, de type decimal également)

            // todo 04 - Appel de la fonction "Addition"
            //            * Appeler la fonction ici pour l'utiliser
            //            * Placer le résultat dans une variable
            //            * Afficher le résultat dans une boîte de dialogue

        }

        private void btnSoustraction_Click(object sender, EventArgs e)
        {
            // toto 05 - Créer la fonction appelée "Soustraction"
            //           elle prend en paramètre deux valeurs A et B de type "decimal"
            //           et retourne la soustraction des deux valeurs (donc, de type decimal également)

            // todo 06 - Appel de la fonction "Soustraction"
            //            * Appeler la fonction ici
            //            * Placer le résultat dans une variable
            //            * Afficher le résultat dans une boîte de dialogue

        }

        private void btnMultiplication_Click(object sender, EventArgs e)
        {
            // toto 07 - Créer la procédure appelée "Multiplication"
            //           elle prend en paramètre deux valeurs A et B de type "decimal" en entrée
            //                                   et une variable result de type "decimal" en sortie (out)
            //           La procédure ne retourne pas de valeur (normal, c'est une procédure !)
            //           elle calcule la multiplication des deux valeurs et l'enregistre dans le paramètre en sortie

            // todo 08 - Appel de la procédure "Multiplication"
            //            * Déclarer une variable résultat de type "decimal"
            //            * Appeler la procédure ici
            //            * Afficher le résultat dans une boîte de dialogue

        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            // toto 09 - Créer la procédure appelée "Division"
            //           elle prend en paramètre deux valeurs A et B de type "decimal" en entrée
            //                                   et une variable result de type "decimal" en sortie (out)
            //           La procédure ne retourne pas de valeur (normal, c'est une procédure !)
            //           elle calcule la division des deux valeurs et l'enregistre dans le paramètre en sortie

            // todo 10 - Appel de la procédure "Division"
            //            * Déclarer une variable résultat de type "decimal"
            //            * Appeler la procédure ici
            //            * Afficher le résultat dans une boîte de dialogue

        }

        private void btnIncrement_Click(object sender, EventArgs e)
        {
            // toto 11 - Créer la procédure appelée "Increment"
            //           elle prend en paramètre une valeur "value" type "decimal" en entrée / sortie (ref)
            //           La procédure ne retourne pas de valeur (normal, c'est une procédure !)
            //           elle incrémente le paramètre en entrée / sortie

            // todo 12 - Appel de la procédure "Increment"
            //            * Déclarer une variable résultat de type "decimal"
            //            * Donner la valeur 5 à la variable résultat;
            //            * Appeler la procédure ici
            //            * Afficher le résultat dans une boîte de dialogue

        }

        private void btnSaluer_Click(object sender, EventArgs e)
        {
            // toto 13 - Créer la procédure appelée "Saluer"
            //           elle prend en paramètre un tableau de chaîne de caractères appelé "noms" en entrée
            //           ajouter devant le paramètre le mot clé "params"
            //           cela permet d'indiquer qu'à partir de ce paramètre
            //           tous les paramètres réels fournis à la procédure seront stocké dans cette variable
            //           (ici c'est à partir du premier paramètre puisqu'il n'y en a qu'un)
            //           Cette fonctionnalité permet de donner un nombre "infini" et indéfini de paramètres
            //           
            //           La procédure ne retourne pas de valeur (normal, c'est une procédure !)
            //           elle affiche un message de type "Bonjour nom !" pour chaque valeur fournie en paramètre

            // todo 14 - Appel de la procédure "Saluer"
            //            * Appeler la procédure ici avec les paramètres "John", "Henri", "Bernard"

        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            // todo 15 - Créer la procedure "Modifier"
            //           qui prend un paramètre en entrée de type "TextBox" appelé "textBox" (avec un t minuscule)
            //           La procédure ne retourne pas de valeur (normal, c'est une procédure !)
            //           La procédure remplace le texte de la TextBox par "modifié !"

            // todo 16 - Appel de la procédure "Modifier"
            //            * Créer une variable TextBox appelée txtBox
            //            * Affecter à cette variable la textBox1 (de la fenêtre)
            //            * Appeler la procédure ici en lui fournissant en paramètre d'entrée la variable txtBox
            //           Rappel : 
            //              Un paramètre d'entrée est une variable stockée dans l'espace mémoire
            //              de la fonction ou de la procédure. Elle va recevoir la copie du
            //              paramètre réel qui a été fourni au moment de l'appel.

            // todo 17 - Petite Enigme :
            //           Observer ce qui se passe quand la procédure "Modifier" est déclenchée
            //           Ecrire sous forme de commentaire ce que vous observez
            //           Et expliquer pourquoi on obtient ce résultat

        }
    }
}
